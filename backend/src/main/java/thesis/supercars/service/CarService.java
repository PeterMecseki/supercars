package thesis.supercars.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import thesis.supercars.persistence.domain.Car;
import thesis.supercars.persistence.domain.Feature;
import thesis.supercars.persistence.repository.CarRepository;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarService {

    private CarRepository carRepository;

    @Autowired
    public void setCarRepository(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public List<Car> getFilteredCarsByBrand(Pageable pageable, String brand, Feature[] features) {
        return carRepository.findCarsByBrand(pageable, brand).stream()
                .filter(car -> car.getFeatures().containsAll(Arrays.asList(features)))
                .collect(Collectors.toList());
    }

    public List<Car> getFilteredCarsByBrandAndModel(Pageable pageable, String brand, String model, Feature[] features) {
        return carRepository.findCarsByBrandAndModel(pageable, brand, model).stream()
                .filter(car -> car.getFeatures().containsAll(Arrays.asList(features)))
                .collect(Collectors.toList());
    }

    public List<Car> getAllCarsByFilters(Pageable pageable, Feature[] features) {
        return carRepository.findAll(pageable).stream()
                .filter(car -> car.getFeatures().containsAll(Arrays.asList(features)))
                .collect(Collectors.toList());
    }
}
