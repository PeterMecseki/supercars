package thesis.supercars.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import thesis.supercars.persistence.domain.Car;
import thesis.supercars.persistence.domain.Feature;
import thesis.supercars.persistence.domain.SavedCar;
import thesis.supercars.persistence.domain.User;
import thesis.supercars.persistence.repository.CarRepository;
import thesis.supercars.persistence.repository.FeatureRepository;
import thesis.supercars.persistence.repository.SavedCarRepository;
import thesis.supercars.persistence.repository.UserRepository;
import thesis.supercars.security.CurrentUser;
import thesis.supercars.security.UserPrincipal;
import thesis.supercars.service.CarService;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.data.domain.Sort.Direction.ASC;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/public")
public class CarController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private CarService carService;
    private CarRepository carRepository;
    private FeatureRepository featureRepository;
    private SavedCarRepository savedCarRepository;
    private UserRepository userRepository;

    @Autowired
    public void setCarService(CarService carService) {
        this.carService = carService;
    }

    @Autowired
    public void setCarRepository(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Autowired
    public void setFeatureRepository(FeatureRepository featureRepository) {
        this.featureRepository = featureRepository;
    }

    @Autowired
    public void setSavedCarRepository(SavedCarRepository savedCarRepository) {
        this.savedCarRepository = savedCarRepository;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "/savedcars", method = POST)
    @ResponseBody
    @Transactional
//    @PreAuthorize("hasRole('ROLE_USER')")
    public List<Car> getSavedCars(@CurrentUser UserPrincipal currentUser) {
        List<SavedCar> savedCars = savedCarRepository.findByUserId(currentUser.getId());
        return savedCars
                .stream()
                .map(SavedCar::getCar)
                .collect(Collectors.toList());

    }

    @RequestMapping(value = "/savedcars/update/{carId}", method = POST)
    @ResponseBody
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<?> updateCarFromFavourites(@PathVariable Long carId,
                                                     @CurrentUser UserPrincipal currentUser) {
        SavedCar savedCar = savedCarRepository.findOneByUserIdAndCarId(currentUser.getId(), carId);
        if (savedCar == null) {
            Car car = carRepository.findCarById(carId);
            User user = userRepository.findById(currentUser.getId()).get();
            SavedCar savedCar1 = new SavedCar(user, car);
            return ResponseEntity.ok().body(savedCarRepository.save(savedCar1).getCar());
        } else {
            savedCarRepository.delete(savedCar);
            Car deletedCar = savedCar.getCar();
            return ResponseEntity.status(HttpStatus.PARTIAL_CONTENT).body(deletedCar);
        }
    }

    @RequestMapping(value = "/cars")
    public List<Car> getAllCars(@PageableDefault(value = 2, page = 0, sort = "price", direction = ASC) Pageable pageable) {
        Page carPages = carRepository.findAll(pageable);
        return carPages.getContent();
    }

    @RequestMapping(value = "/carscount")
    public long getAllCarsCount(@PageableDefault(value = 2, page = 0, sort = "price", direction = ASC) Pageable pageable) {
        Page carPages = carRepository.findAll(pageable);
        return carPages.getTotalElements();
    }

    @RequestMapping("/brands")
    public List<String> getBrandNames() {
        List<String> brands = carRepository.getAllCarBrands();
        return brands;
    }



    @RequestMapping("/models/{brand}")
    public List<String> getModelNamesByBrand(@Valid @PathVariable String brand) {
        List<String> models = carRepository.getAllModelsByBrand(brand);
        return models;
    }

    @RequestMapping("/features")
    public List<Feature> getFeatureNames() {
        List<Feature> features = featureRepository.findAll();
        return features;
    }

    @RequestMapping(value = "/carbrands/", params = "brand", method = POST)
    @ResponseBody
    public List<Car> getFilteredCarsByBrand(@RequestParam("brand") String brand,
                                            @RequestBody Feature[] features,
                                            @PageableDefault(value = 2, page = 0, sort = "price", direction = ASC) Pageable pageable) {

        List<Car> cars = carService.getFilteredCarsByBrand(pageable, brand, features);
        return new PageImpl<>(cars, pageable, cars.size()).getContent();
    }

    @RequestMapping(value = "/carbrandsmodels/", params = {"brand", "model"}, method = POST)
    @ResponseBody
    public List<Car> getFilteredCarsByBrandAndModel(@RequestParam("brand") String brand,
                                                    @RequestParam("model") String model,
                                                    @RequestBody Feature[] features,
                                                    @PageableDefault(value = 2, page = 1, sort = "price", direction = ASC) Pageable pageable) {

        List<Car> cars;
        if (brand.isEmpty() && model.isEmpty()) {
            cars = carService.getAllCarsByFilters(pageable, features);
        } else if (model.isEmpty()) {
            cars = carService.getFilteredCarsByBrand(pageable, brand, features);
        } else {
            cars = carService.getFilteredCarsByBrandAndModel(pageable, brand, model, features);
        }
        return new PageImpl<>(cars, pageable, cars.size()).getContent();
    }

    @GetMapping("/car/{id}")
    ResponseEntity<?> getCarById(@PathVariable Long id) {
        Optional<Car> car = carRepository.findById(id);
        return car.map(response -> ResponseEntity.ok().body(response))
                .orElse(new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }

    @PostMapping("/addcar")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    ResponseEntity addCar(@Valid @RequestBody Car car) {
        Car result = carRepository.save(car);
        try {
            logger.info("Car with id: " + result.getId() + "has been added.");
            return ResponseEntity.created(new URI("/addcar/" + result.getId())).body("Car successfully added.");
        } catch (URISyntaxException e) {
            logger.error("Cannot create URL for new car entity: ", e);
        }
        return ResponseEntity.unprocessableEntity().build();
    }

    @PutMapping("/car/{id}")
    @Secured("ROLE_USER")
    ResponseEntity editCar(@Valid @RequestBody Car car, @PathVariable Long id) {
        car.setId(id);
        logger.info("Car with id: " + id + "has been edited.");
        return ResponseEntity.ok().body(carRepository.save(car));
    }

    @Transactional
    @DeleteMapping("/car/{id}")
    @Secured("ROLE_USER")
    public ResponseEntity deleteCar(@PathVariable Long id) {
        carRepository.deleteCarById(id);
        logger.info("Car with id: " + id + "has been deleted.");
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
