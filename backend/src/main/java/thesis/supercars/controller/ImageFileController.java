package thesis.supercars.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import thesis.supercars.payload.UploadImageFileResponse;
import thesis.supercars.persistence.domain.ImageFile;
import thesis.supercars.service.ImageFileStorageService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/uploadArea")
@RestController
public class ImageFileController {

    private static final Logger logger = LoggerFactory.getLogger(ImageFileController.class);

    @Autowired
    private ImageFileStorageService imageFileStorageService;

    @PostMapping("/imageUpload")
    public UploadImageFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
        ImageFile imageFile = imageFileStorageService.storeFile(file);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path("/downloadFile/")
                .path(imageFile.getId())
                .toUriString();

        return new UploadImageFileResponse(imageFile.getFileName(), fileDownloadUri,
                file.getContentType(), file.getSize());
    }

    @PostMapping("/multipleImagesUpload")
    public List<UploadImageFileResponse> uploadMultipleFiles(@RequestParam("files") MultipartFile[] files) {
        return Arrays.asList(files)
                .stream()
                .map(file -> uploadFile(file))
                .collect(Collectors.toList());
    }

    @GetMapping("/downloadImage/{fileId}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileId) {
        ImageFile imageFile = imageFileStorageService.getFile(fileId);

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(imageFile.getFileType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + imageFile.getFileName() + "\"")
                .body(new ByteArrayResource(imageFile.getData()));
    }


}