package thesis.supercars.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import thesis.supercars.persistence.domain.Feature;

@Repository
public interface FeatureRepository extends JpaRepository<Feature, Long> {

    Feature findFeatureById(Long id);
}
