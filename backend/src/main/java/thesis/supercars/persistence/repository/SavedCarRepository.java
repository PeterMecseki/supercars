package thesis.supercars.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import thesis.supercars.persistence.domain.SavedCar;

import java.util.List;

public interface SavedCarRepository extends JpaRepository<SavedCar,Long> {
    List<SavedCar> findByUserId(Long userId);

    SavedCar findOneByUserIdAndCarId(Long userId, Long carId);
}
