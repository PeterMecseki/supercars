package thesis.supercars.persistence.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import thesis.supercars.persistence.domain.Role;
import thesis.supercars.persistence.domain.RoleDef;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleDef roleDef);
}