package thesis.supercars.persistence.domain;

public enum RoleDef {
    ROLE_USER,
    ROLE_SELLER,
    ROLE_ADMIN
}
