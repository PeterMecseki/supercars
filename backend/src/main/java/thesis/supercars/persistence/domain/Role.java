package thesis.supercars.persistence.domain;

import org.hibernate.annotations.NaturalId;

import javax.persistence.*;

@Entity
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NaturalId
    @Enumerated(EnumType.STRING)
    @Column(length = 50)
    private RoleDef name;

    public Role() {

    }

    public Role(RoleDef name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public RoleDef getName() {
        return name;
    }

    public void setName(RoleDef name) {
        this.name = name;
    }
}
