package thesis.supercars.persistence.domain;

import org.hibernate.annotations.NaturalId;
import thesis.supercars.persistence.audit.DateAuditor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "\"user\"")
public class User extends DateAuditor {

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private Long id;

    @Column(unique = true)
    @NaturalId
    @Size(max = 50)
    private String username;

    @Size(max = 30)
    private String firstName;
    @Size(max = 30)
    private String lastName;


    @Column(unique = true)
    @NaturalId
    @Size(max = 50)
    @Email
    private String email;

    @Size(max = 20)
    private String phoneNumber;

    @Size(max = 100)
    private String password;


    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private Address address;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<SavedCar> savedCars;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    private User() {

    }

    public User(@Size(max = 50) String username, @Size(max = 50) @Email String email, @Size(max = 100) String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }


    public User(@Size(max = 50) String username, @Size(max = 30) String firstName, @Size(max = 30) String lastName, @Size(max = 50) @Email String email, @Size(max = 20) String phoneNumber, @Size(max = 100) String password, Address address, List<SavedCar> savedCars, Set<Role> roles) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.address = address;
        this.savedCars = savedCars;
        this.roles = roles;
    }

    public User(@Size(max = 50) @Email String email, @Size(max = 100) String password) {
        this.email = email;
        this.password = password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String userName) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public List<SavedCar> getSavedCars() {
        return savedCars;
    }

    public void setSavedCars(List<SavedCar> savedCars) {
        this.savedCars = savedCars;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", password='" + password + '\'' +
                ", address=" + address +
                ", savedCars=" + savedCars +
                ", roles=" + roles +
                '}';
    }
}

