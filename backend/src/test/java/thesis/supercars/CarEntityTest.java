package thesis.supercars;

import org.junit.Test;
import thesis.supercars.persistence.domain.Car;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class CarEntityTest {

    @Test
    public void testCar() {
        Car car = new Car("Ford",
                "Focus",
                "Sedan",
                22000,
                new Date(2015 - 05 - 01),
                "Metal Blue", 6500000,
                "Szép állapot");

        assertTrue("model is Focus", "Focus".equals(car.getModel()));
        assertTrue("kilometer is 22000", 22000 == car.getKilometer());
        assertFalse(car.getBodyType().equals("Kombi"));
        assertFalse(car.getBrand().equals("Toyota"));
        assertThat(car.getFirstRegistration()).isEqualToIgnoringHours(new Date(2015 - 05 - 01));
        assertThat(car.getPrice() == 6500000);
    }
}

