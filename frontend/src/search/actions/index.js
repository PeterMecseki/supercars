import { get, post } from 'axios';

import { GET_ALL_CARS, GET_ALL_CAR_BRANDS, GET_MODELS_BY_BRAND, SET_SELECTED_CAR_BRAND, SET_SELECTED_CAR_MODEL, FILTER_CARS, SET_FILTER_DEFAULTS, SET_ACTIVE_PAGE } from './types';
import { GET_ALL_CARS_URL, GET_ALL_CAR_BRANDS_URL, GET_MODELS_BY_BRAND_URL, FILTER_CARS_URL, } from './urls';

export function getAllCars() {
    return async dispatch => {
        return new Promise(function (resolve, reject) {
            get(
                GET_ALL_CARS_URL,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            ).then(response => {
                return resolve(dispatch(loadAllCars(response.data)));
            }).catch((error) => {
                return reject(error);
            });
        });
    }
}

export function getAllCarBrands() {
    return async dispatch => {
        return new Promise(function (resolve, reject) {
            get(
                GET_ALL_CAR_BRANDS_URL,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            ).then(response => {
                return resolve(dispatch(loadAllCarBrands(response.data)));
            }).catch((error) => {
                return reject(error);
            });
        });
    }
}

export function getModelsByBrand(brand) {
    return async dispatch => {
        return new Promise(function (resolve, reject) {
            const url = GET_MODELS_BY_BRAND_URL+brand;

            get(
                url,
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            ).then(response => {
                return resolve(dispatch(loadAllModelsByBrand(response.data)));
            }).catch((error) => {
                return reject(error);
            });
        });
    }
}

export function changeSelectedCarBrand(brand) {
    return async dispatch => {
        return new Promise(function (resolve, reject) {
            dispatch(setSelectedCarModel(null));
            return resolve(dispatch(setSelectedCarBrand(brand)));
        });
    }
}

export function setSelectedCarBrand(brand) {
    return {
        type: SET_SELECTED_CAR_BRAND,
        payload: brand
    }
}

export function filterCars() {
    return async (dispatch, getState) => {
        return new Promise(function (resolve, reject) {
            const { cars } = getState();
            const model = !cars.selectedModel ? "all" : cars.selectedModel;
            let filterData = `{ "brand": "${cars.selectedBrand}", "model": "${model}"}`;

            post(
                FILTER_CARS_URL,
                JSON.parse(filterData),
                {
                    headers: {
                        'Content-Type': 'application/json',
                    }
                }
            ).then(response => {
                return resolve(dispatch(loadAllCars(response.data)));
            }).catch((error) => {
                return reject(error);
            });
        });
    }
}

export function setFilterDefaults(filters) {
    return {
        type: SET_FILTER_DEFAULTS,
        payload: filters
    }
}

export function setSelectedCarModel(model) {
    return {
        type: SET_SELECTED_CAR_MODEL,
        payload: model
    }
}

function loadAllModelsByBrand(models) {
    return {
        type: GET_MODELS_BY_BRAND,
        payload: models
    }
}

function loadAllCars(cars) {
    return {
        type: GET_ALL_CARS,
        payload: cars
    }
}

function loadAllCarBrands(brands) {
    return {
        type: GET_ALL_CAR_BRANDS,
        payload: brands
    }
}
export function setActivePage(page) {
    return function (dispatch) {
        dispatch({
            type: SET_ACTIVE_PAGE,
            payload: page
        })
    }
}
