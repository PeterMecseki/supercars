import React, {Component} from 'react';

import {getAllCars, filterCars, setFilterDefaults} from '../actions';

import SelectField from './SelectField';
import SelectCarModelField from './SelectCarModelField';
import CarBox from "./CarBox";

import connect from "react-redux/es/connect/connect";

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSpinner} from "@fortawesome/free-solid-svg-icons";
import Pager from "./Pager";

class Search extends Component {

    constructor(props) {
        super(props);

        this.state = {
            selectedBrand: null,
            isLoading: false,
            selectedCarType: null,
            cars: null
        }

        this.search = this.search.bind(this);
        this.selectCarType = this.selectCarType.bind(this);
    }

    componentDidMount() {
        this.setFilterDefaults();
        this.setState({
            isLoading: true,
        });
        this.props.getAllCars().then(result => {
            this.setState({
                isLoading: false,
                cars: result
            });
        }).catch(error => {
            //console.log('ERROR: ', error);
        });
    }

    setFilterDefaults() {
        const filter = {
            "brand": null,
            "model": null
        }

        this.props.setFilterDefaults(filter);
    }

    search() {
        this.props.filterCars().then(res => {
            console.log('filtered: ', res);
        });
    }

    selectCarType(selectedCarType) {
        this.setState({
            selectedBrand: selectedCarType
        });
    }

    getSearchForm() {
        return (
            <div className='col bg-info p-2'>
                <SelectField selectCarType={this.selectCarType}/>
                {this.props.selectedBrand && <SelectCarModelField/>}

                <div className='feature-set mt-4'>
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1"/>
                        <label className="form-check-label" htmlFor="inlineCheckbox1">Garancia</label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2"/>
                        <label className="form-check-label" htmlFor="inlineCheckbox2">Szervizkönyves</label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1"/>
                        <label className="form-check-label" htmlFor="inlineCheckbox1">Xenon</label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2"/>
                        <label className="form-check-label" htmlFor="inlineCheckbox2">Tempomat</label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1"/>
                        <label className="form-check-label" htmlFor="inlineCheckbox1">ABS</label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2"/>
                        <label className="form-check-label" htmlFor="inlineCheckbox2">ASR</label>
                    </div>
                    <div className="form-check form-check-inline">
                        <input className="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1"/>
                        <label className="form-check-label" htmlFor="inlineCheckbox1">ESP</label>
                    </div>
                    <button type="button"
                            className="btn btn-primary float-lg-right mr-lg-1"
                            onClick={this.search}>Keresés
                    </button>
                </div>
            </div>
        );
    }

    refreshCourses(page) {
        console.log('page: ', page);
    }

    getBoxes() {
        return (
            <div className='col'>
                <Pager boxesPerPage='5' refreshCourses={this.refreshCourses} />
                {
                    this.props.cars.map((car) => {
                        const carName = car.brand + ' ' + car.model;
                        return <CarBox key={car.id}
                                       carId={car.id}
                                       carType={carName}
                                       firstRegistration={car.firstRegistration}
                                       engine={car.engine}
                                       prize={car.price}
                                       kilometer={car.kilometer}
                                       description='ÚJ AUTÓ 2+3 ÉV gyári garanciával, 2 év ingyenes Assistance szolgáltatással. A Mirror szériafelszereltségen felül az...'/>
                    })
                }
                <Pager boxesPerPage='5' />
            </div>
        );
    }

    render() {
        return (
            <div className='container'>
                <div className='row'>
                    {this.getSearchForm()}
                </div>
                <div className="row">
                    {this.state.isLoading &&
                    <div className='col mt-4 mb-4 text-center'><FontAwesomeIcon icon={faSpinner} className='fa fa-2x'
                                                                                spin/></div>}
                </div>
                <div className='row'>
                    {this.props.cars && this.getBoxes()}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        selectedBrand: state.cars.selectedBrand,
        cars: state.cars.cars
    }
}

export default connect(mapStateToProps, {getAllCars, filterCars, setFilterDefaults})(Search);