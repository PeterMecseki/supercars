import React, {Component} from 'react';
import connect from "react-redux/es/connect/connect";
import {login, registration} from "../actions";


class LoginPage extends Component {

    constructor(props) {
        super(props);

        this.LOGIN = 'LOGIN';
        this.REGISTER = 'REGISTER';

        this.state = {
            formType: this.LOGIN,
            username: null,
            usernameOrEmail: null,
            email: null,
            password: null,
            passwordAgain: null,
            message: null
        }
    }

    changeFormType() {
        this.setState({
            formType: this.state.formType == this.LOGIN ? this.REGISTER : this.LOGIN
        });
    }

    isLoginForm() {
        return this.state.formType == this.LOGIN;
    }

    getFormFields() {
        let fields = [];

        if (this.isLoginForm()) {
            fields.push(
                <input type="text"
                       onChange={(event) => this.changeUsernameOrEmail(event.target.value)}
                       className="form-control"
                       aria-describedby="usernameOrEmail"
                       placeholder="Felhasználónév vagy email"
                       key="usernameOrEmail"
                       autoComplete="usernameOrEmail"/>
            );
        }

        if (!this.isLoginForm()) {
            fields.push(
                <input type="text"
                       onChange={(event) => this.changeUsername(event.target.value)}
                       className="form-control"
                       aria-describedby="username"
                       placeholder="Felhasználónév"
                       key="username"
                       autoComplete="username"/>
            );
        }

        if (!this.isLoginForm()) {
            fields.push(
                <input type="email"
                       onChange={(event) => this.changeEmail(event.target.value)}
                       className="form-control"
                       aria-describedby="email"
                       placeholder="Email cím"
                       key="email"
                       autoComplete="email"/>
            );
        }

        fields.push(
            <input type="password"
                   onChange={(event) => this.changePassword(event.target.value)}
                   className="form-control"
                   aria-describedby="password"
                   placeholder="Jelszó"
                   key="password"
                   autoComplete="password"/>
        );

        if (!this.isLoginForm()) {
            fields.push(
                <input type="password"
                       onChange={(event) => this.changePasswordAgain(event.target.value)}
                       className="form-control"
                       aria-describedby="password-again"
                       placeholder="Jelszó újra"
                       key="password-again"
                       autoComplete="password-again"/>
            );
        }

        return fields;
    }

    getFormSubmitButton() {
        const style = "btn btn-primary btn-block mt-4 mb-4";

        if (this.isLoginForm()) {
            return <button className={style} onClick={(event) => this.login(event)}>Bejelentkezés</button>;
        } else {
            return <button className={style} onClick={(event) => this.registration(event)}>Regisztráció</button>;
        }
    }

    getChangeFormTypeLink() {
        if (this.isLoginForm()) {
            return <p onClick={() => this.changeFormType()}>Regisztráció</p>;
        } else {
            return <p onClick={() => this.changeFormType()}>Bejelentkezés</p>;
        }
    }

    getFormTitle() {
        const style = "h4 mb-3 font-weight-normal text-center mt-4 mb-4";
        return <h1 className={style}>{this.isLoginForm() ? "Bejelentkezés" : "Regisztráció"}</h1>;
    }

    getForm() {
        return (
            <div className="form-group">
                {this.getFormTitle()}
                {this.getFormFields()}
                {this.getFormSubmitButton()}
                {this.getChangeFormTypeLink()}
            </div>
        );
    }

    changeUsername(username) {
        this.setState({
            username
        });
    }

    changeUsernameOrEmail(usernameOrEmail){
        this.setState({
            usernameOrEmail
        });
    }

    changeEmail(email) {
        this.setState({
            email
        });
    }

    changePassword(password) {
        this.setState({
            password
        });
    }

    changePasswordAgain(passwordAgain) {
        this.setState({
            passwordAgain
        });
    }

    registration(event) {
        event.preventDefault();

        const registrationData = {
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
        }
        this.props.registration(registrationData).then(response => {
            this.changeMessage(response);
        });
    }

    login(event) {
        event.preventDefault();

        const loginData = {
            usernameOrEmail: this.state.usernameOrEmail,
            password: this.state.password
        }
        this.props.login(loginData).then(response => {
            this.changeMessage(response);
        });
    }

    changeMessage(message) {
        this.setState({
            message: message
        });
    }

    render() {
        return (
            <div className="container">
                <div className="row">
                    <div className="col"></div>
                    <div className="col-auto mt-4" id="box">
                        {this.state.message &&
                        <div className="bg-info">
                            {this.state.message}
                        </div>
                        }
                        <form>
                            {this.getForm()}
                        </form>
                    </div>
                    <div className="col"></div>
                </div>
            </div>
        );
    }
}

export default connect(null, {login, registration})(LoginPage);