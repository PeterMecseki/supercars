import React, { PureComponent } from 'react';

export default class TextAreaField extends PureComponent {

    constructor(props) {
        super(props);

        this.state = {
            value: ''
        }
    }

    changeValue(value) {
        this.setState({
            value
        });
        this.props.onChange(value);
    }

    getField() {
        return (
            <textarea   value={this.state.value} 
                        className="form-control"
                        rows="5"
                        id={this.props.name} 
                        onChange={(event) => this.changeValue(event.target.value)} />
        );
    }

    render() {
        return (
            <div className='form-group'>
                <label htmlFor={this.props.name}>{this.props.label}</label>
                {this.getField()}
            </div>
        );
    }

}