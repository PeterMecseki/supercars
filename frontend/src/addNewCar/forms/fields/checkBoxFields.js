import React, { PureComponent, Fragment } from 'react';

export default class CheckBoxField extends PureComponent {

    constructor(props) {
        super(props);

        this.state = {

        }
    }

    changeValue(value) {
        this.setState({

        });
    }

    getFields() {
        return this.props.values.map(value => {
            return (
                <div className='form-check' key={value}>
                    <input type="checkbox" className='form-check-input' id={value} name={value} value={value} />
                    <label htmlFor={value} className='form-check-label'>{value}</label>
                </div>
            );
        })
    }

    render() {
        return (
            <Fragment>
                <h2 className='mb-4'>{this.props.label}</h2>
                {this.getFields()}
            </Fragment>
        );
    }

}