import {SET_CAR_TITLE} from "../../search/actions/types";

export function setCarTitle(title) {
    return async function (dispatch) {
        return dispatch({
            type: SET_CAR_TITLE,
            payload: title
        });
    };
}