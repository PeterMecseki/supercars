import {SET_CAR_TITLE} from "../actions/types";

const initialState = {
    carTitle: ''
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_CAR_TITLE:
            return { ...state, carTitle: action.payload };
        default:
            return state;
    }
}