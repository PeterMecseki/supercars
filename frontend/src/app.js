import React, {Component} from 'react';
import {Route, Switch} from 'react-router-dom';

import Search from './search/components/Search';
import CarDetailsPage from "./carDetails/components/CarDetailsPage";
import LoginPage from "./login/components/LoginPage";
import AddNewCarPage from "./addNewCar/components/AddNewCarPage";

export default class App extends Component {

    render() {
        return (
            <div>
                {this.props.children}
                <Switch>
                    <Route exact={true} path='/' component={Search}/>
                    <Route path='/car/:carId' component={CarDetailsPage}/>
                    <Route path='/login' component={LoginPage}/>
                    <Route path='/add' component={AddNewCarPage}/>
                </Switch>
            </div>
        );
    }
}
