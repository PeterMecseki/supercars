import React, {PureComponent, useCallback} from 'react';
import {useDropzone} from 'react-dropzone';

export default function MyDropzone(props) {

    const onDrop = useCallback(acceptedFiles => {
        acceptedFiles.forEach(file => {
            file.url = URL.createObjectURL(file)
            props.addImageToPreviews(file);
        });
    }, [])

    const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop})

    return (
        <div className="area mt-4" {...getRootProps()}>
            <input {...getInputProps()} />
            <p>Húzd ide a fotókat vagy kattints ide a tallózó ablak megnyitásához</p>
        </div>
    )
}