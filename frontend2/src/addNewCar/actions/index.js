import {ADD_CAR_IMAGE, REMOVE_CAR_IMAGE, SET_PRIZE, SET_DESCRIPTION} from './types';

export function setCarPrize(prize) {
    return async function (dispatch) {
        return dispatch({
            type: SET_PRIZE,
            payload: prize
        });
    };
}

export function setCarDescription(description) {
    return async function (dispatch) {
        return dispatch({
            type: SET_DESCRIPTION,
            payload: description
        });
    };
}

export function addCarImage(image) {
    return async function (dispatch) {
        return dispatch({
            type: ADD_CAR_IMAGE,
            payload: image
        });
    };
}

export function removeCarImage(imageName) {
    return async function (dispatch) {
        return dispatch({
            type: REMOVE_CAR_IMAGE,
            payload: imageName
        });
    };
}