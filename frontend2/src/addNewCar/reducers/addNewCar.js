import {ADD_CAR_IMAGE, REMOVE_CAR_IMAGE, SET_PRIZE, SET_DESCRIPTION} from "../actions/types";

const initialState = {
    carImages: []
}

export default function (state = initialState, action) {
    switch (action.type) {
        case SET_PRIZE: {
            return {...state, prize: action.payload};
        }
        case SET_DESCRIPTION: {
            return {...state, description: action.payload};
        }
        case ADD_CAR_IMAGE: {
            const images = state.carImages;
            images.push(action.payload);
            return {...state, carImages: images};
        }
        case REMOVE_CAR_IMAGE: {
            const images = state.carImages.filter(image => image.name !== action.payload);
            return {...state, carImages: images};
        }
        default:
            return state;
    }
}