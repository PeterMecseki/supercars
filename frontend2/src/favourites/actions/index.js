import {GET_FAVOURITE_CARS, UPDATE_FAVOURITE_CARS, UPDATE_CAR_FROM_FAVOURITES} from "./types";
import {get, post} from "axios";
import {GET_FAVOURITE_CARS_URL, UPDATE_CAR_FROM_FAVOURITES_URL} from "./urls";

export function getFavouriteCars() {
    return async dispatch => {
        return new Promise(function (resolve, reject) {
            const url = GET_FAVOURITE_CARS_URL;
            const token = localStorage.getItem("token");
            post(
                url,
                token,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    }
                }
            ).then(response => {
                return resolve(dispatch(loadAllFavouriteCars(response.data)));
            }).catch((error) => {
                return reject(error);
            });
        });
    }
}

function loadAllFavouriteCars(favouriteCars) {
    return ({
        type: GET_FAVOURITE_CARS,
        payload: favouriteCars
    })
}

function changeFavouriteCars(favouriteCar, direction) {
    return ({
        type: UPDATE_FAVOURITE_CARS,
        payload: favouriteCar,
        direction
    })
}

export function updateCarFromFavourites(carId) {
    return async dispatch => {
        return new Promise(function (resolve, reject) {
            const url = UPDATE_CAR_FROM_FAVOURITES_URL + carId;
            const token = localStorage.getItem("token");
            post(
                url,
                token,
                {
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': 'Bearer ' + token
                    }
                }
            ).then(response => {
                let direction = 'ADD';

                if(response.status !== 200) {
                    direction = 'REMOVE';
                }

                return resolve(dispatch(changeFavouriteCars(response.data, direction)));
            }).catch((error) => {
                return reject(error);
            });
        });
    }
}