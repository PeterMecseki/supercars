import React, {Component, Fragment} from 'react';
import connect from "react-redux/es/connect/connect";
import Pager from "../../search/components/Pager";
import CarBox from "../../search/components/CarBox";
import {getFavouriteCars, updateCarFromFavourites} from "../actions";

class FavouriteCarsPage extends Component {

    constructor(props) {
        super(props);

        this.state = {}
    }

    componentDidMount() {
        this.props.getFavouriteCars();
    }

    getBoxes() {
        return (
            <div className='col'>
                {/*<Pager boxesPerPage='5' refreshCars={this.refreshCars}/>*/}
                {
                    this.props.favouriteCars && this.props.favouriteCars.map((car) => {
                        return <CarBox key={car.id}
                                       car={car}
                                       description='ÚJ AUTÓ 2+3 ÉV gyári garanciával, 2 év ingyenes Assistance szolgáltatással. A Mirror szériafelszereltségen felül az...'/>
                    })
                }
                {/*<Pager boxesPerPage='5'/>*/}
            </div>
        );
    }


    render() {
        return (
            <div className='container'>
                <div className='row'>
                    {this.getBoxes()}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        favouriteCars: state.favourite.cars
    }
}

export default connect(mapStateToProps, {getFavouriteCars, updateCarFromFavourites})(FavouriteCarsPage);