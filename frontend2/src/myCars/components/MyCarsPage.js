import React, {Component, Fragment} from 'react';
import connect from "react-redux/es/connect/connect";

class MyCarsPage extends Component {

    constructor(props) {
        super(props);

        this.state = {}
    }


    render() {
        return (
            <div className='container'>
                <div className='row'>
                    <div className='col'>MyCars</div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        selectedBrand: state.cars.selectedBrand,
        cars: state.cars.cars
    }
}

export default connect(mapStateToProps, {})(MyCarsPage);