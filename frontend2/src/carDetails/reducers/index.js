import {GET_CAR_DETAILS} from "../actions/types";

export default function (state = {}, action) {
    switch (action.type) {
        case GET_CAR_DETAILS:
            return {...state, car: action.payload};
        default:
            return state;
    }
}