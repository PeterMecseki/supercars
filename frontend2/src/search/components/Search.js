import React, {Component, Fragment} from 'react';
import connect from "react-redux/es/connect/connect";

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSpinner} from "@fortawesome/free-solid-svg-icons";

import CarBox from "./CarBox";
import Pager from "./Pager";
import DoubleSelectField from '../../addNewCar/forms/fields/doubleSelectField';
import CheckBoxFields from '../../addNewCar/forms/fields/checkBoxFields';

import {getAllCars, filterCars, setFilterDefaults, refreshCars, getAllFeatures} from '../actions';
import {getFavouriteCars} from '../../favourites/actions/index';

class Search extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
        }

        this.changeLoadingIndicator = this.changeLoadingIndicator.bind(this);
        this.refreshCars = this.refreshCars.bind(this);
    }

    componentDidMount() {
        this.props.getAllFeatures();
        this.refreshCars();
    }

    refreshCars() {
        this.changeLoadingIndicator();

        Promise.all([
            this.props.refreshCars(this.props.selectedBrand, this.props.selectedModel, this.props.features, this.props.activePage),
            this.props.getFavouriteCars()
        ]).finally(res => {
            this.changeLoadingIndicator();
        });
    }

    changeLoadingIndicator() {
        this.setState({
            isLoading: !this.state.isLoading,
        });
    }

    getSearchForm() {
        return (
            <Fragment>
                <div className="row">
                    <div className='col-sm-12 col-md-4 pl-3 pr-3 first-filter-box'>
                        <DoubleSelectField/>
                    </div>
                    <div className='col-md-8 pl-4 second-filter-box'>
                        {this.props.allFeatures &&
                        <CheckBoxFields name='fetures' label='Felszereltség' values={this.props.allFeatures}/>}
                    </div>
                </div>
                <div className="row">
                    <div className='col text-right pl-3 pr-3 pt-2 pb-2 first-filter-box'>
                        <button className='btn btn-info' onClick={this.refreshCars}>Keresés</button>
                    </div>
                </div>
            </Fragment>
        )
    }

    getBoxes() {
        return (
            <div className='col'>
                <Pager boxesPerPage='1' refreshCars={this.refreshCars}/>
                {
                    this.props.cars.map((car) => {
                        return <CarBox key={car.id} car={car}/>
                    })
                }
                <Pager boxesPerPage='1' refreshCars={this.refreshCars}/>
            </div>
        );
    }

    render() {
        return (
            <div className='container'>
                {this.getSearchForm()}
                <div className="row">
                    {this.state.isLoading &&
                    <div className='col mt-4 mb-4 text-center'><FontAwesomeIcon icon={faSpinner} className='fa fa-2x'
                                                                                spin/></div>}
                </div>
                <div className="row">
                    <div className="col pt-2 pb-2 first-filter-box mt-1 mb-1">Találatok
                        száma: {this.props.carCount}</div>
                </div>
                <div className='row'>
                    {this.props.cars && this.props.favouriteCars && this.getBoxes()}
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        selectedBrand: state.cars.selectedBrand,
        selectedModel: state.cars.selectedModel,
        features: state.filter.features,
        allFeatures: state.filter.allFeatures,
        favouriteCars: state.favourite.cars,
        cars: state.cars.cars,
        carCount: state.cars.allCount,
        activePage: state.cars.activePage
    }
}

export default connect(mapStateToProps, {
    getAllCars,
    filterCars,
    setFilterDefaults,
    getFavouriteCars,
    refreshCars,
    getAllFeatures
})(Search);