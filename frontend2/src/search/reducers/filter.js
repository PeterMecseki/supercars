import {
    CHANGE_FEATURES_VALUES, GET_ALL_FEATURES
} from '../actions/types';

export default function (state = {}, action) {
    switch (action.type) {
        case GET_ALL_FEATURES: {
            return {...state, allFeatures: action.payload};
        }
        case CHANGE_FEATURES_VALUES: {
            let features = state.features ? state.features : [];

            if (features.indexOf(action.payload) >= 0) {
                features = features.filter(feature => {
                    return feature.id != action.payload.id;
                });
            } else {
                features.push(action.payload);
            }

            return {...state, features};
        }
        default:
            return state;
    }
}