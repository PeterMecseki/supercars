import {
    GET_ALL_CAR_BRANDS, GET_ALL_CARS, GET_MODELS_BY_BRAND, SET_SELECTED_CAR_BRAND,
    SET_SELECTED_CAR_MODEL, SET_FILTER_DEFAULTS, SET_ACTIVE_PAGE, SET_SELECTED_BRAND_AND_MODEL,
    CHANGE_FEATURES_VALUES
} from '../actions/types';

import {read_cookie, bake_cookie} from 'sfcookies';

const initState = {
    activePage: 1
}

export default function (state = initState, action) {
    switch (action.type) {
        case GET_ALL_CARS:
            return {...state, cars: action.payload, allCount: action.payload.length};
        case GET_ALL_CAR_BRANDS:
            return {...state, brands: action.payload};
        case GET_MODELS_BY_BRAND:
            return {...state, models: action.payload};
        case SET_ACTIVE_PAGE:
            return {...state, activePage: action.payload};
        case SET_SELECTED_CAR_BRAND: {
            bake_cookie('brand', action.payload);
            return {...state, selectedBrand: action.payload, selectedModel: null};
        }
        case SET_SELECTED_CAR_MODEL: {
            bake_cookie('model', action.payload);
            return {...state, selectedModel: action.payload};
        }
        case SET_SELECTED_BRAND_AND_MODEL: {
            return {...state, selectedModel: action.payload.model, selectedBrand: action.payload.brand};
        }
        case SET_FILTER_DEFAULTS: {
            let brand = read_cookie('brand').length ? read_cookie('brand') : action.payload.brand;
            let model = read_cookie('model').length ? read_cookie('model') : action.payload.model;
            return {...state, selectedBrand: brand, selectedModel: model};
        }
        default:
            return state;
    }
}