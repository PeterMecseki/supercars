import React, {Component} from 'react';
import {authenticateUser} from '../actions/index';

export const AuthenticatorContext = React.createContext(null);

const AuthenticateUser = (ComposedComponent) => {
    class Authenticate extends Component {
        state = {
            token: null
        };

        async componentDidMount() {
            try {
                const {status, token} = await authenticateUser();
                if (status === 'OK') {
                    this.setState({token});
                }
            } catch (err) {
                console.log('error', err);
            }
        }

        render() {
            const {token} = this.state;
            return (
                <AuthenticatorContext.Provider value={token}>
                    <ComposedComponent/>
                </AuthenticatorContext.Provider>
            );
        }
    }

    return Authenticate;
};

export default AuthenticateUser;